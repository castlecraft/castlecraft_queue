from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in castlecraft_queue/__init__.py
from castlecraft_queue import __version__ as version

setup(
	name="castlecraft_queue",
	version=version,
	description="Interface",
	author="Castlecraft",
	author_email="test@test.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
