import frappe
from frappe import _
import json

@frappe.whitelist()
def interface_dump():
	if frappe.request.__dict__["environ"]["REQUEST_METHOD"] == "GET":
		uuid = frappe.request.__dict__["args"]["uuid"]
		obj = frappe.get_doc('Interface', uuid)
		if obj.doctype_reference and obj.document_reference:
			data = frappe.get_doc(obj.doctype_reference,obj.document_reference)
			return data
		else:
			return frappe.throw("Data does not Exists")
	data = json.loads(frappe.request.__dict__["_cached_data"])
	doc = frappe.new_doc("Interface")
	doc.json = str(data.get("json"))
	doc.source_app_name = data.get("details", {}).get("source_app_name")
	doc.target_app_name = data.get("details", {}).get("target_app_name")
	doc.request_type = data.get("details", {}).get("request_type")
	doc.endpoint = data.get("details", {}).get("endpoint")
	doc.master_doctype = data.get("json", {}).get("doctype")
	doc.request_method = frappe.request.__dict__["environ"]["REQUEST_METHOD"]
	doc.save()
	frappe.db.commit()
	return doc.name

@frappe.whitelist()
def tp():
	return "hello"