# Copyright (c) 2021, Castlecraft and contributors
# For license information, please see license.txt

import frappe
from frappe import _
import json
from frappe import exceptions
from frappe.model.document import Document
import traceback
import uuid
import requests

class Interface(Document):
	# def validate(self):
	# 	url = "http://54.210.240.18/sskMart/api/erpcheck"
	# 	headers = {
	# 		"ERPToken":"6wuVpCsYEB5gXhuJixyQTk6voBCPFcZfnGcgquQTLVWO5hEpQGJk71NH8fknWMAjbroY7zZFme3tZUL8eJTqfrZyAgbvGOuBc5on1RtJgcTKUvqz7VgM1ruAGSoOWaWf74OV2Ii2LDkAi5coTgmWrkTUzMS60TE3nhGpY1Guzc6gCHlCg2r6klw22HYSV2oKK5fVG52kaSUFqm6hw2o1WbLbQSM2asoKVX6kqeOCkJzdfIQ4jN0DRIM0EKFCyTD1",
	# 		"Content-Type":"multipart/form-data"
	# 	}
	# 	data = {
	# 		"erpuuid":"812f221e-2c9e-4984-b362-4f2e8fadd724",
	# 		"doctype":"Item Group",
	# 		"docstatus":"failed"
	# 	}
	# 	response = requests.post(url,headers=headers, json=data)
	# 	print("_______________")
	# 	print("_______________")
	# 	print("_______________")
	# 	print("_______________")
	# 	print(response.__dict__)
	# 	print(json.loads(response.__dict__.get("_content")))
	# 	print(type(json.loads(response.__dict__.get("_content"))))
	# 	# oc_responce = json.loads(response.__dict__)
	# 	self.oc_server_response = str(json.loads(response.__dict__.get("_content")))

	def after_insert(self):
		frappe.enqueue(master_creation, doc=self)

	def autoname(self):
			self.name = str(uuid.uuid4())

def master_creation(doc):
	doc_1 = None
	if doc.request_method == "POST":
		if doc.request_type == "Resource":
			try:
				data = doc.json.replace('"','\\"')
				json_data = json.loads(data.replace("'",'"'))
			except Exception as e:
				doc.error = traceback.format_exc()
				doc.status = "Failed"
				# status_to_oc = send_status(doc)
				# doc = status_to_oc
				doc.save()
				frappe.db.commit()

			try:
				data = doc.json.replace('"','\\"')
				json_data = json.loads(data.replace("'",'"'))
				doc_1 = frappe.get_doc(json_data)
				doc_1.save()
				doc.status = "Success"
				doc.document_reference = doc_1.name
				doc.doctype_reference = doc_1.doctype
				# status_to_oc = send_status(doc)
				# doc = status_to_oc
				doc.save()
				frappe.db.commit()
				
			except Exception as e:
				data = doc.json.replace('"','\\"')
				json_data = json.loads(data.replace("'",'"'))
				exist = frappe.db.exists(json_data)
				if doc_1 != None and not exist:
					doc_1.delete()
				doc.error = traceback.format_exc()
				doc.status = "Failed"
				# status_to_oc = send_status(doc)
				# doc = status_to_oc
				doc.save()
				frappe.db.commit()
		else:
			try:
				json_data = json.loads(doc.json.replace("'",'"'))
				module = frappe.get_module(".".join(doc.endpoint.split(".")[:-1]))
				method = doc.endpoint.split(".")[-1]
				a = getattr(module,method)()
				doc.te = a
				doc.status = "Success"
				# status_to_oc = send_status(doc)
				# doc = status_to_oc
				doc.save()
				frappe.db.commit()
				
			except Exception as e:
				exist = frappe.db.exists(json_data)
				if doc_1 != None and not exist:
					doc_1.delete()
				doc.error = traceback.format_exc()
				doc.status = "Failed"
				# status_to_oc = send_status(doc)
				# doc = status_to_oc
				doc.save()
				frappe.db.commit()

	elif doc.request_method == "DELETE":
		try:
			json_data = json.loads(doc.json.replace("'",'"'))
			obj = frappe.get_doc('Interface', json_data.get("interface_id"))
			frappe.delete_doc(obj.doctype_reference,obj.document_reference)
			doc.status = "Success"
			# status_to_oc = send_status(doc)
			# doc = status_to_oc
			doc.interface_id = json_data.get("interface_id")
			doc.save()
			frappe.db.commit()


		except Exception as e:
			doc.error = traceback.format_exc()
			doc.status = "Failed"
			# status_to_oc = send_status(doc)
			# doc = status_to_oc
			doc.save()
			frappe.db.commit()
	
	elif doc.request_method == "PUT":
		try:
			json_data = json.loads(doc.json.replace("'",'"'))
			obj = frappe.get_doc('Interface', json_data.get("interface_id"))
			doc_to_chng = frappe.get_doc(obj.doctype_reference,obj.document_reference)
			for i in list(json_data.get("fields")):
				setattr(doc_to_chng, i, json_data.get("fields")[i])
			doc_to_chng.save()
			doc.interface_id = json_data.get("interface_id")
			doc.status = "Success"
			# status_to_oc = send_status(doc)
			# doc = status_to_oc
			doc.save()
			frappe.db.commit()


		except Exception as e:
			doc.error = traceback.format_exc()
			doc.status = "Failed"
			# status_to_oc = send_status(doc)
			# doc = status_to_oc
			doc.save()
			frappe.db.commit()

# def send_status(doc):
	# pass
	# try:
	# 	url = "http://18.204.194.99/sskMart/api/erpcheck"
	# 	headers = {
	# 		"ERPToken":"6wuVpCsYEB5gXhuJixyQTk6voBCPFcZfnGcgquQTLVWO5hEpQGJk71NH8fknWMAjbroY7zZFme3tZUL8eJTqfrZyAgbvGOuBc5on1RtJgcTKUvqz7VgM1ruAGSoOWaWf74OV2Ii2LDkAi5coTgmWrkTUzMS60TE3nhGpY1Guzc6gCHlCg2r6klw22HYSV2oKK5fVG52kaSUFqm6hw2o1WbLbQSM2asoKVX6kqeOCkJzdfIQ4jN0DRIM0EKFCyTD1",
	# 		"Content-Type":"multipart/form-data"
	# 	}
	# 	data = {
	# 		"erpuuid":doc.name,
	# 		"doctype":doc.doctype_reference,
	# 		"docstatus":doc.status
	# 	}
	# 	response = requests.post(url,headers=headers, json=data)
	# 	oc_response = str(json.loads(response.__dict__.get("_content")))
	# 	doc.oc_server_response = oc_response
	# 	return doc
	
	# except Exception as e:
	# 	doc.oc_server_response = traceback.format_exc()
	# 	return doc