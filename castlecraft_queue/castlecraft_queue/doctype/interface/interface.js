// Copyright (c) 2021, Castlecraft and contributors
// For license information, please see license.txt

frappe.ui.form.on('Interface', {
	refresh: function(frm) {
		if(frm.doc.status == "Failed"){
			frm.add_custom_button(__("Retry"),function () {
				frm.copy_doc();
				frm.doc.error = ""
				frm.doc.status = "Processing"
				frm.refresh_fields()
			  }
			);
		}
	}
});
