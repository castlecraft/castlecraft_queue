from frappe import _

def get_data():
	return [
		{
			"module_name": "Castlecraft Queue",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Castlecraft Queue")
		}
	]
